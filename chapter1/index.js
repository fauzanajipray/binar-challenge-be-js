const readline = require('readline');
var rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

const kalkulator = () => {
    console.clear();
    console.log();
    console.log("===========================================");
    console.log("                 Kalkulator                ");
    console.log("===========================================");
    console.log();
    console.log("1. Tambah  (+)");
    console.log("2. Kurang  (-)");
    console.log("3. Bagi    (/)");
    console.log("4. Kali    (*)");
    console.log("5. Akar Quadrat");
    console.log("6. Luas Persegi    (cm)");
    console.log("7. Volume Kubus    (cm)");
    console.log("8. Volume Tabung   (cm)");
    console.log("9. Keluar");
    console.log();
    
    const questionClose = () => {
        console.log();
        console.log("===========================================");
        console.log();
        rl.question("Kembali ke menu utama ? (y/n) : ", (pilihan) => {
            if(pilihan == "y" || pilihan == "Y"){
                kalkulator();
            }else{
                rl.close();
            }
        });
    }

    rl.question("Masukkan Pilihan : " , (pilihan) => {
        console.log("Anda Memilih Menu  " + pilihan );
        if(pilihan == 1 || pilihan == 2 || pilihan == 3 || pilihan == 4){
            rl.question("Masukan Bilangan pertama : ", (jawabanpertama) => {
                rl.question("Masukan Bilangan kedua : ", (jawabankedua) => {
                    console.log("-----------------------------------------");
                    console.log("Bilangan Pertama Anda  " + jawabanpertama );
                    console.log("Bilangan Kedua Anda  " + jawabankedua );
                    console.log("-----------------------------------------");
                    if(pilihan == "1"){
                        console.log("Hasilnya :" + (parseInt(jawabanpertama) + parseInt(jawabankedua)));
                    }else if(pilihan == "2"){
                        console.log("Hasilnya :" + (parseInt(jawabanpertama) - parseInt(jawabankedua)));
                    }else if(pilihan == "3"){
                        console.log("Hasilnya :" +(parseInt(jawabanpertama) / parseInt(jawabankedua)));
                    }else if(pilihan == "4"){
                        console.log("Hasilnya :" +(parseInt(jawabanpertama) * parseInt(jawabankedua)))   
                    }else{
                        console.log("Pilihan anda tidak tersedia");
                    }
                    questionClose();
                });     
            });
        }else if(pilihan == 5){
            rl.question("Masukan Bilangan : ", (jawaban) =>{
                console.log("-----------------------------------------");
                console.log("Bilangan Anda  " + jawaban );
                console.log("-----------------------------------------");
                console.log("Hasilnya :" + (Math.sqrt(parseInt(jawaban))));
                questionClose();
            });
        }else if(pilihan == 6){
            rl.question("Masukan Sisi : ", (sisi) => {
                console.log("-----------------------------------------");
                console.log("Sisi Anda  " + sisi );
                console.log("-----------------------------------------");
                console.log("Hasilnya :" + (parseInt(sisi) * parseInt(sisi)));
                questionClose();
            });
        }else if(pilihan == 7){
            rl.question("Masukan Sisi : ", (sisi) => {
                console.log("-----------------------------------------");
                console.log("Sisi Anda  " + sisi );
                console.log("-----------------------------------------");
                console.log("Hasilnya :" + (parseInt(sisi) * parseInt(sisi) * parseInt(sisi)));
                questionClose();
            });
        }else if(pilihan == 8){
            rl.question("Masukan Jari-jari : ", (jari) => {
                rl.question("Masukan Tinggi : ", (tinggi) => {
                    console.log("-----------------------------------------");
                    console.log("Jari-jari Anda  " + jari );
                    console.log("-----------------------------------------");
                    console.log("Hasilnya :" + (parseInt(jari) * parseInt(jari) * parseInt(tinggi) * 3.14) );
                    questionClose();
                });
            });
        }else if(pilihan == 9){
            rl.close();
        }else{
            console.log("Pilihan Tidak Tersedia");
            questionClose();
        }
    });

    rl.on('close', () => {
        console.log();
        console.log("===========================================");
        console.log(" Terima Kasih Telah Menggunakan Kalkulator ");
        console.log("===========================================");
        console.log();
    })
}

kalkulator();


