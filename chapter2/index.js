const { HitungNilai } = require('./HitungNilai');


console.log();
console.log("=====================================================");
console.log("                     Nilai Siswa                     ");
console.log("=====================================================");
console.log();

const htNilai = new HitungNilai();
htNilai.inputArray().then(() => {
    console.log();
    console.log("=====================================================");
    console.log();
    console.log("Nilai                    : " + htNilai.nilai);
    console.log("Nilai rata-rata          : " + htNilai.rataRata());
    console.log("Nilai Urutan             : " + htNilai.urutkan());
    console.log("Nilai Terendah           : " + htNilai.terendah());
    console.log("Nilai Tertinggi          : " + htNilai.tertinggi());
    console.log("KKM                      : "+ htNilai.nilaiKKM);
    console.log("Jumlah Siswa Lulus       : "+ htNilai.jumlahLulus());
    console.log("Jumlah Siswa Tidak Lulus : "+ htNilai.jumlahTidakLulus());
    console.log();
    console.log("=====================================================");
});



