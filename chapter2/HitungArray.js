class HitungArray {

    constructor() {
        if (this.constructor === HitungArray) {
            throw new TypeError('Cannot construct Abstract instances directly');
        }
    }

    #input() { throw new Error('Abstract method'); }

    #inputArray() { throw new Error('Abstract method'); }

    hitung() { throw new Error('Abstract method'); }

    rataRata() { throw new Error('Abstract method');}

    urutkan() { throw new Error('Abstract method');}

    terendah() { throw new Error('Abstract method');}

    tertinggi() { throw new Error('Abstract method');}
}

module.exports = { HitungArray };