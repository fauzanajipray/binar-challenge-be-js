const mergeSort = require('./mergeSort');
const readline = require('readline');
const { HitungArray } = require('./HitungArray');

var rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

class HitungNilai extends HitungArray {

    static nilaiKKM = 75;
    arr = [];

    get nilai() {
        return this.arr;
    }

    get nilaiKKM() {
        return HitungNilai.nilaiKKM;
    }

    #input(array) {
        return new Promise((resolve) => {
            rl.question("Masukkan beberapa nilai siswa (q untuk keluar) : ", (nilai) => {
                if(nilai == 'q'){
                    resolve(array)
                    rl.close();
                } else {
                    array.push(parseInt(nilai));
                    this.#input(array).then(resolve);
                }
            });
        })
    }

    async inputArray() {
        return this.#input([]).then((array) => {
            this.arr = array;
        })
    }

    urutkan() {
        return mergeSort(this.arr)
    }
    
    rataRata() {
        let total = 0;
        for (let i = 0; i < this.arr.length; i++) {
            total += this.arr[i];
        }
        return total / this.arr.length;
    }

    terendah(){
        let nilaiUrut = this.urutkan();
        return nilaiUrut[0];
    }

    tertinggi(){
        let nilaiUrut = this.urutkan();
        return nilaiUrut[nilaiUrut.length - 1];
    }
    
    jumlahLulus(){
        let nilaiUrut = this.urutkan();
        let count = 0;
        for (let i = 0; i < nilaiUrut.length; i++){
            if (nilaiUrut[i] >= this.nilaiKKM){
                count++
            }
        }
        return count;
    }

    jumlahTidakLulus(){
        return this.arr.length - this.jumlahLulus()
    }

}

module.exports = { HitungNilai };